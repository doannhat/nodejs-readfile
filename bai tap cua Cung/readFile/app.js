var fs = require("fs");

fs.readdir("./input/", function (err, files) {
    if (err) {
        return console.error(err);
    }
    if (fs.existsSync('./output/output.txt')) {
        fs.unlink('./output/output.txt', function (err) {
            if (err) {
                return console.error(err);
            }
            console.log('=============== OUTPUT ================');
            fs.appendFileSync('./output/output.txt', '=============== OUTPUT ================');
            files.forEach(function (file) {
                let content = fs.readFileSync('./input/' + file);
                fs.appendFileSync('./output/output.txt',
                    `
Name: ${file}
Content: ${content}
=======================================`);
            });
        });
    } else {
        console.log('=============== OUTPUT ================');
        fs.appendFileSync('./output/output.txt', '=============== OUTPUT ================');
        files.forEach(function (file) {
            let content = fs.readFileSync('./input/' + file);
            fs.appendFileSync('./output/output.txt',
                `
Name: ${file}
Content: ${content}
=======================================`);
        });
    }

});
