const http = require('http');

const configs = require('./Configs/config');
const helper = require('./Configs/helper');

http.createServer(helper.onRequest).listen(configs.port);