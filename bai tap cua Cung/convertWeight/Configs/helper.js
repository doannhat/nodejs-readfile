function convert_kg_lb(param){
    return (param*2.2046).toFixed(2);
}

function convert_oz_lb(param){
    return (param*0.062500).toFixed(2);
}

function convert_g_lb(param){
    return (param*0.0022046).toFixed(2);
}

module.exports = {
    onRequest: function onRequest(req, res) {
        // Show Console
        console.log('=========== WEIGHT CONVERT TO LB ==========');
        let kg = 10;
        console.log('Input: '+kg + ' kg');
        console.log('Output: '+convert_kg_lb(kg) +' lb');
        console.log('--------------------------------------');
        let oz = 500;
        console.log('Input: '+oz + ' oz');
        console.log('Output: '+convert_oz_lb(oz) +' lb');
        console.log('--------------------------------------');
        let g = 3000;
        console.log('Input: '+g + ' g');
        console.log('Output: '+convert_g_lb(g) +' lb');

        // Show Browser
        res.write('<html><body><div>');
        res.write('<h1>=========== WEIGHT CONVERT TO LB ==========</h1>');
        res.write('<h2>Input: '+kg + ' kg </h2>');
        res.write('<h2>Output: '+convert_kg_lb(kg) +' lb </h2>');
        res.write('<h2>--------------------------------------</h2>');
        res.write('<h2>Input: '+oz + ' oz </h2>');
        res.write('<h2>Output: '+convert_oz_lb(oz) +' lb </h2>');
        res.write('<h2>-------------------------------------- </h2>');
        res.write('<h2>Input: '+g + ' g </h2>');
        res.write('<h2>Output: '+convert_g_lb(g) +' lb </h2>');
        res.write('</div></body></html>');

        res.end();
    }
}