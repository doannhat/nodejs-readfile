var http = require('http');
var converter = require('./converter_modules');
var fs = require('fs');
var a = 10;

var b = 500;

var c = 3000;

http.createServer(function(req, res){

    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(`
    <p>=========== WEIGHT CONVERT TO LB ==========</p>
    <p>Input: ${a} kg</p>
    <p>Output: ${converter.kgConverter(a)} lb</p>
    <p>--------------------------------------</p>
    <p>Input:	${b} oz</p>
    <p>Output:	${converter.ozConverter(500)} lb</p>
    <p>--------------------------------------</p>
    <p>Input:	${c} g</p>
    <p>Output:	${converter.gConverter(3000)} lb</p>
    `);
   

}).listen(8080);

